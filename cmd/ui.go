/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"

	//	"github.com/bitfield/script"
	"github.com/adrg/xdg"
	"github.com/spf13/cobra"
)

// uiCmd represents the ui command
var uiCmd = &cobra.Command{
	Use:   "ui",
	Short: "Run git helper for dotfiles",
	Long: `Start a git helper (gitui [^1] by default) with the work tree and
bare repo configured. This is equivalent to:

~~~
gitui -w $HOME -d $XDG_DATA_HOME/dotfiles
~~~

    ---

    [^1]: Stephan D., https://github.com/extrawurst/gitui
`,
	Run: func(cmd *cobra.Command, args []string) {
		sh_cmd := exec.Command("gitui", "-d", Dotfiles_dir(), "-w", xdg.Home)
		sh_cmd.Stdin = os.Stdin
		sh_cmd.Stderr = os.Stderr
		sh_cmd.Stdout = os.Stdout
		err := sh_cmd.Run()
		if err != nil {
            log.Fatalf("Unable to run git helper:\n\t%s\n", err)

		}
        reader := bufio.NewWriter(sh_cmd.Stderr)
		output, _ := reader.WriteString("\n")
		fmt.Fprintln(os.Stderr, output)
	},
}

func init() {
	rootCmd.AddCommand(uiCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// uiCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// uiCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
