/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"os"
	"github.com/adrg/xdg"
	"github.com/spf13/cobra"
    "path/filepath"
)


func Dotfiles_dir() string {
    return filepath.Clean(xdg.DataHome + "/dotfiles/")
}



// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "bare_dot_go",
	Short: "Git wrapper to manage dotfiles with a bare repository",
	Long: `Simplified commands to help create and manage a bare git repository
of dot files

Examples
  ~~~sh
  ` + os.Args[0] + ` init gitlab.com/username/dotfiles
  ` + os.Args[0] + ` add $XDG_CONFIG_DIRS/nvim/init.lua
  ` + os.Args[0] + ` ui
  ~~~
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.bare_dot_go.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}


