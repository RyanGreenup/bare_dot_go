/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"bufio"
	"fmt"
	"strings"

	"os"
	"os/exec"

	"github.com/adrg/xdg"
	"log"

	"github.com/spf13/cobra"
)

// gdCmd represents the gd command
var gdCmd = &cobra.Command{
	Use:   "gd",
	Short: "A wrapper around git, pass all git commands here",
	Long: `Allows git commands to be passed with a seperate working tree and
bare git repository, essentially the same as:

~~~sh
alias gd='git --git-dir=` + xdg.DataHome + `/dotfiles --work-tree=` + xdg.Home + `'
~~~
    `,
	Run: func(cmd *cobra.Command, args []string) {

		sh_cmd := exec.Command("git", "--git-dir", Dotfiles_dir(), "--work-tree", xdg.Home, strings.Join(args, ""))
		sh_cmd.Stdin = os.Stdin
		sh_cmd.Stderr = os.Stderr
		sh_cmd.Stdout = os.Stdout
		err := sh_cmd.Run()
		if err != nil {
			log.Fatalf("Unable to run git:\n\t%s\n", err)

		}
		reader := bufio.NewWriter(sh_cmd.Stderr)
		output, _ := reader.WriteString("\n")
		fmt.Fprintln(os.Stderr, output)

	},
}

func init() {
	rootCmd.AddCommand(gdCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// gdCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// gdCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
