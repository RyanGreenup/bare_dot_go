/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"log"
	"os"

    "github.com/adrg/xdg"
	"github.com/bitfield/script"
	"github.com/go-git/go-git/v5" // with go modules enabled (GO111MODULE=on or outside GOPATH)
	"github.com/spf13/cobra"
)

const (
	// NoSubsection token is passed to Config.Section and Config.SetSection to
	// represent the absence of a section.
	NoSubsection = ""
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialise repository of dotfiles on new machine",
	Long: `Creates a bare git repository with the work-tree set as $HOME,
configured to not show untracked files. **If** the ~git~ repository is provided
it will be cloned and configured, otherwise the user will need to set
the upstream origin.

Equivalent to:

~~~sh
mkdir $XDG_DATA_HOME/dotfiles
git init --bare $HOME/.dotfiles
git --git-dir=$XDG_DATA_HOME/dotfiles \
    config --local status.showUntrackedFiles no
~~~

or if a repo is set the logic should be similar to this:

~~~sh
# Create a temp dir
tmpdotfiles="$(mktemp -d)"

# Clone the repo to the temp directory
git clone --separate-git-dir=$XDG_DATA_HOME/dotfiles \
[repo] "${tmpdotfiles}"

# don't show untracked files
git --git-dir=$XDG_DATA_HOME/dotfiles \
    config --local status.showUntrackedFiles no

# copy the dotfiles into home
rsync --recursive --verbose --exclude '.git' "${tmpdotfiles}"/ $HOME/
rm -r "${tmpdotfiles}"
~~~`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			initialize_new_repo()
		} else if len(args) == 1 {
			initialize_existing_repo(args[0])
		} else {
			log.Fatal("Provide only 1 argument as the repo or leave empty to create a new repo")
		}
	},
}

func initialize_existing_repo(url string) {
	// Clone files to temp_dir
	temp_dir, err := os.MkdirTemp(os.TempDir(), "")
	if err != nil {
		log.Fatal(err)
	}
	out, err := script.Exec(fmt.Sprintf("git clone --separate-git-dir=%s %s %s", Dotfiles_dir(), url, temp_dir)).String()
	if err != nil {
		log.Fatalf("Unable to clone repo:\n\t%s\t%s\n\n", out, err)
	}

	// Set the git repo as bare and untracked
	r, err := git.PlainOpen(Dotfiles_dir())
	if err != nil {
		log.Fatalf("Despite just cloning the repository, at %s, there has been an error opening it :\n\t%s\n\n", Dotfiles_dir(), err)
	}

	set_untracked_files(r)
	set_bare_repo(r)
	fmt.Printf("\n\nCloned Bare Repository (showUntrackedFiles=no) to %s\n\n", Dotfiles_dir())

	// Move files into home
	out, err = script.Exec(fmt.Sprintf("rsync --recursive --verbose --exclude '.git' %s/ %s/", temp_dir, xdg.Home)).String()
	fmt.Println(out)
	if err != nil {
		log.Fatal(err)
	}

    fmt.Printf("\nSuccessfully copied files: \n\t %s  -->  %s\n", Dotfiles_dir(), xdg.Home)
}

func initialize_new_repo() {
	fmt.Printf("Attempting to Initialise\n\n")
	r, err := git.PlainInit(Dotfiles_dir(), true)
	if err != nil {
		log.Fatalf("Unable to initialize new repository:\n\t%s\n\n", err)
	}

	set_untracked_files(r)

	fmt.Printf("New repository initialized in: \n\t%s\n\n", Dotfiles_dir())

}

func set_untracked_files(r *git.Repository) {

	c, err := r.Config()
	if err != nil {
		log.Fatal(err)
	}

	c.Raw.AddOption("status", NoSubsection, "showUntrackedFiles", "no")
	err = c.Validate()
	if err != nil {
		log.Fatal(err)
	}
	err = r.SetConfig(c)
	if err != nil {
		log.Fatal(err)
	}

}

func set_bare_repo(r *git.Repository) {

	c, err := r.Config()
	if err != nil {
		log.Fatal(err)
	}

	c.Core.IsBare = true
	err = c.Validate()
	if err != nil {
		log.Fatal(err)
	}
	err = r.SetConfig(c)
	if err != nil {
		log.Fatal(err)
	}

}

func init() {
	rootCmd.AddCommand(initCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// initCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// initCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
