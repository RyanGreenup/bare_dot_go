/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import (
	"bare_dot_go/cmd"
	"github.com/adrg/xdg"
	"path/filepath"
)

func Dotfiles_dir() string {
    return filepath.Clean(xdg.Home + "/Notes/")
}

func main() {
	cmd.Execute()
}
